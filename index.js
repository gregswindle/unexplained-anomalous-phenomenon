const { getSubtitles } = require('youtube-captions-scraper')
const analyzer = require('./src/captions/analyzer/')
// const youtubeCaptions = require('./tests/__mocks__/s02/e03/captions/youtube-captions.json')

/**
 * STARTLING EXPERIMENT Proves Alien Civilization | The Secret of Skinwalker
 * Ranch (S2) | History.
 */
const VIDEO_ID = `NE4L8NhCkto`

const main = async () => {
  try {
    const captions = await getSubtitles({
      videoID: VIDEO_ID
    })

    let results = analyzer.findWords(captions)
    results = analyzer.groupMatchedWords(results)
    console.log(results)
  } catch (err) {
    console.error(err)
  }
}

main()
