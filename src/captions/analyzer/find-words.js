const defaultOptions = require('./default-options.json')

const findWords = (subtitles = [], words = []) => {
  const NOT_FOUND = 0
  const stems = [
    ...defaultOptions.words,
    ...words
  ]
  let results = []
  try {
    subtitles.forEach((subtitle) => {
      stems.forEach(stem => {
        const pattern = new RegExp(stem, 'gim')
        const matches = Array.from(subtitle.text.matchAll(pattern))
        if (matches.length !== NOT_FOUND) {
          results.push(Object.assign({
            stem
          }, subtitle))
        }
      })
    })
    results = results.flatMap(value => value)
  } catch (err) {
    console.error(err)
    results = [err]
  }
  return results
}

module.exports = findWords
