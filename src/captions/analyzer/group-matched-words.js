const groupBy = require('lodash.groupby')

const groupMatchedWords = ((subtitles = [], propertyName = 'stem') =>
  groupBy(subtitles, propertyName))

module.exports = groupMatchedWords
