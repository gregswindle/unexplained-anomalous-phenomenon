const findWords = require('./find-words')
const groupMatchedWords = require('./group-matched-words')

module.exports = {
  findWords,
  groupMatchedWords
}
