const { getSubtitles } = require('youtube-captions-scraper')
const defaultOptions = require('./default-options.json')


const getCaptionsById = async (videoId, options = {
  'lang': 'en'
}) => {
  const opts = Object.apply({
    'videoID': videoId
  }, defaultOptions, options)
  console.log(opts)
  try {
    const subtitles = await getSubtitles(opts)
    console.log(subtitles)
    return JSON.stringify(subtitles)
  } catch (err) {
    console.error(err)
    return JSON.stringify(err)
  }
}

module.exports = {
  getCaptionsById
}
